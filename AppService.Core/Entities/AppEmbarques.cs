﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class AppEmbarques
    {
        public decimal Id { get; set; }
        public int? IdCategoria { get; set; }
        public string NombreCategoria { get; set; }
        public int? IdSubCategoria { get; set; }
        public string NombreSubCategoria { get; set; }
        public decimal? TotalVenta { get; set; }
    }
}
