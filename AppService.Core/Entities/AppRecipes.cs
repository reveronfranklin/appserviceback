﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class AppRecipes
    {
        public int Id { get; set; }
        public int? AppproductsId { get; set; }
        public int? AppVariableId { get; set; }
        public string Description { get; set; }
        public int? AppIngredientsId { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? TotalCost { get; set; }
        public string Formula { get; set; }
        public string FormulaValue { get; set; }
        public bool? SumValue { get; set; }
        public int? OrderCalculate { get; set; }
        public string Code { get; set; }
        public bool? IncludeInSearch { get; set; }
        public int? Secuencia { get; set; }
        public bool? AfectaCosto { get; set; }

        public virtual AppIngredients AppIngredients { get; set; }
        public virtual AppVariables AppVariable { get; set; }
        public virtual AppProducts Appproducts { get; set; }
    }
}
