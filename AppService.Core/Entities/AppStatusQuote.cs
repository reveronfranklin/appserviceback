﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class AppStatusQuote
    {
        public AppStatusQuote()
        {
            AppDetailQuotes = new HashSet<AppDetailQuotes>();
            AppGeneralQuotes = new HashSet<AppGeneralQuotes>();
        }

        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string FlagGanada { get; set; }
        public string FlagModificar { get; set; }
        public string FlagActiva { get; set; }
        public string FlagPostergada { get; set; }
        public string FlagPerdida { get; set; }
        public string FlagCaducada { get; set; }
        public string FlagEnEspera { get; set; }
        public string FlagCaducaInactiva { get; set; }
        public string Abreviado { get; set; }
        public string PrimeraEstacion { get; set; }
        public decimal? CondicionRazonId { get; set; }

        public virtual ICollection<AppDetailQuotes> AppDetailQuotes { get; set; }
        public virtual ICollection<AppGeneralQuotes> AppGeneralQuotes { get; set; }
    }
}
