﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class AppUnitsofMeasureforIngredients
    {
        public int Id { get; set; }
        public int? AppIngredientsId { get; set; }
        public int? AppUnitsId { get; set; }
        public decimal? Numerator { get; set; }
        public decimal? Denominator { get; set; }
        public string UserCreate { get; set; }
        public string UserUpdate { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public virtual AppIngredients AppIngredients { get; set; }
        public virtual AppUnits AppUnits { get; set; }
    }
}
