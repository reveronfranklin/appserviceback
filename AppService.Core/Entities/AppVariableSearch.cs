﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class AppVariableSearch
    {
        public int Id { get; set; }
        public int? AppSubCategoryId { get; set; }
        public int? AppVariableId { get; set; }
        public string SearchText { get; set; }

        public virtual AppSubCategory AppSubCategory { get; set; }
        public virtual AppVariables AppVariable { get; set; }
    }
}
