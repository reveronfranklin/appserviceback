﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class AutoAudit
    {
        public long AuditId { get; set; }
        public DateTime? AuditDate { get; set; }
        public string AuditUserName { get; set; }
        public string TableName { get; set; }
        public string OldContent { get; set; }
        public string NewContent { get; set; }
    }
}
