﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CatCalibrePapelRollos
    {
        public long CatCalibrePapelRollosId { get; set; }
        public string Papel { get; set; }
        public int? Gramaje { get; set; }
        public decimal? Calibre { get; set; }
    }
}
