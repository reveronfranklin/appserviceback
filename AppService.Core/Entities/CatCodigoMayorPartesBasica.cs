﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CatCodigoMayorPartesBasica
    {
        public decimal Id { get; set; }
        public string CodigoMayor { get; set; }
        public int? Partes { get; set; }
        public string BasicaBusqueda { get; set; }
        public decimal? Frecuencia { get; set; }
    }
}
