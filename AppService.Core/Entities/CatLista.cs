﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CatLista
    {
        public int CatListaId { get; set; }
        public long CatMaterialId { get; set; }
        public string CodigoProduct { get; set; }
        public decimal Cantidad { get; set; }
        public decimal IncrementoCantidad { get; set; }
        public decimal PrecioMinimo { get; set; }
        public decimal PrecioMaximo { get; set; }
        public decimal IdCalculo { get; set; }
        public decimal Duracion { get; set; }
        public string Descripcion { get; set; }

        public virtual CatMaterial CatMaterial { get; set; }
        public virtual MtrProducto CodigoProductNavigation { get; set; }
    }
}
