﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CatMedidaBasica1
    {
        public CatMedidaBasica1()
        {
            CatMaterial = new HashSet<CatMaterial>();
        }

        public int CatMedidaBasicaId { get; set; }
        public string MedidaString { get; set; }
        public string MedidaBusqueda { get; set; }
        public decimal MedidaDecimal { get; set; }

        public virtual ICollection<CatMaterial> CatMaterial { get; set; }
    }
}
