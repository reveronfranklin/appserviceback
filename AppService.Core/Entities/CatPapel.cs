﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CatPapel
    {
        public CatPapel()
        {
            CatMaterialDetalleTmp = new HashSet<CatMaterialDetalleTmp>();
        }

        public string CatPapelId { get; set; }
        public string Descripcion { get; set; }
        public bool EsCarbon { get; set; }

        public virtual ICollection<CatMaterialDetalleTmp> CatMaterialDetalleTmp { get; set; }
    }
}
