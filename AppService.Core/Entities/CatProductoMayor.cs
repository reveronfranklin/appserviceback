﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CatProductoMayor
    {
        public long CatProductoMayorId { get; set; }
        public string CodigoProduct { get; set; }
        public string Descripcion { get; set; }
        public int Frecuencia { get; set; }
        public bool? EsRollo { get; set; }
        public string Inactivo { get; set; }
    }
}
