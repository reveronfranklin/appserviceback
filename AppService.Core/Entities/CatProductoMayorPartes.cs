﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CatProductoMayorPartes
    {
        public long CatProductoMayorPartesId { get; set; }
        public long? CatProductoMayorId { get; set; }
        public string CodigoProduct { get; set; }
        public int Partes { get; set; }
        public int Frecuencia { get; set; }
    }
}
