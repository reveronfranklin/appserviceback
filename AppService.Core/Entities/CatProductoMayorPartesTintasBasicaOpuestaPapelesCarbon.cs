﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CatProductoMayorPartesTintasBasicaOpuestaPapelesCarbon
    {
        public long CatProductoMayorPartesTintasBasicaOpuestaPapelesCarbonId { get; set; }
        public long? CatProductoMayorId { get; set; }
        public long? CatProductoMayorPartesId { get; set; }
        public long? CatProductoMayorPartesTintasId { get; set; }
        public long? CatProductoMayorPartesTintasBasicaId { get; set; }
        public long? CatProductoMayorPartesTintasBasicaOpuestaId { get; set; }
        public long? CatProductoMayorPartesTintasBasicaOpuestaPapelesId { get; set; }
        public string CodigoProduct { get; set; }
        public int Partes { get; set; }
        public int Tintas { get; set; }
        public string BasicaBusqueda { get; set; }
        public string OpuestaBusqueda { get; set; }
        public string PapelesBusqueda { get; set; }
        public string CarbonBusqueda { get; set; }
        public int Frecuencia { get; set; }
    }
}
