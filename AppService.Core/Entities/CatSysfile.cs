﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CatSysfile
    {
        public int CatSysfileId { get; set; }
        public string FechaPublicacion { get; set; }
        public string FechaModificacionMateriales { get; set; }
        public bool? InactivarConsultaPrecios { get; set; }
        public int? IdPrograma { get; set; }
    }
}
