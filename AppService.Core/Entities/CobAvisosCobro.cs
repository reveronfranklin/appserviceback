﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CobAvisosCobro
    {
        public decimal Id { get; set; }
        public string Cliente { get; set; }
        public string Usuario { get; set; }
        public string NombreArchivo { get; set; }
        public byte[] Archivo { get; set; }
        public bool Procesado { get; set; }
        public DateTime? Fecha { get; set; }
        public byte[] ArchivotTiff { get; set; }
        public string Html { get; set; }
    }
}
