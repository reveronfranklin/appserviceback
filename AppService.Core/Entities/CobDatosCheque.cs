﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CobDatosCheque
    {
        public long Documento { get; set; }
        public string IdBanco { get; set; }
        public string IdTipoTransaccion { get; set; }
        public decimal MontoCheque { get; set; }
        public long NumCheque { get; set; }
        public DateTime FechaCheque { get; set; }
        public decimal? RmontoCheque { get; set; }

        public virtual CobGeneralCobranza DocumentoNavigation { get; set; }
    }
}
