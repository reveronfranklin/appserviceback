﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CobDescuentoIvaTipoTransaccion
    {
        public short Id { get; set; }
        public string IdTipoTransaccion { get; set; }
        public decimal BsDesde { get; set; }
        public decimal BsHasta { get; set; }
        public decimal PorcDescuento { get; set; }
        public DateTime FechaRegistro { get; set; }
        public decimal? RbsDesde { get; set; }
        public decimal? RbsHasta { get; set; }
    }
}
