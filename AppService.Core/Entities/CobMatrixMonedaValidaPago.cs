﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CobMatrixMonedaValidaPago
    {
        public int Id { get; set; }
        public string MonedaDocumento { get; set; }
        public string MonedaPago { get; set; }
    }
}
