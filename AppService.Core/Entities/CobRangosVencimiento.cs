﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CobRangosVencimiento
    {
        public decimal Id { get; set; }
        public decimal Codigo { get; set; }
        public string Descripcion { get; set; }
        public decimal? DiasDesde { get; set; }
        public decimal? DiasHasta { get; set; }
        public string Coletilla { get; set; }
        public string PieUno { get; set; }
        public string PieDos { get; set; }
    }
}
