﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CobResumenEstadoCuentaTemp
    {
        public decimal Id { get; set; }
        public string Cliente { get; set; }
        public string NombreCliente { get; set; }
        public int? Oficina { get; set; }
        public string NombreOficina { get; set; }
        public string Consultor { get; set; }
        public string NombreConsultor { get; set; }
        public decimal Saldo { get; set; }
        public int? CodigoRango { get; set; }
        public string DescripcionRango { get; set; }
    }
}
