﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CobRolCobranza
    {
        public int IdRolCobranza { get; set; }
        public string IdUsuario { get; set; }
        public short IdOficina { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
