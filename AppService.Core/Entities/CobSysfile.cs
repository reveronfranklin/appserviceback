﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CobSysfile
    {
        public short Id { get; set; }
        public DateTime FechaLm { get; set; }
        public bool? FlagAprobarRc { get; set; }
        public DateTime? FechaLmcxC { get; set; }
    }
}
