﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CobTipoTransaccionRespaldoAntesEliminarColettilla
    {
        public string IdTipoTransaccion { get; set; }
        public string NombreTipoTransaccion { get; set; }
        public bool FlagActivaVerificacion { get; set; }
        public bool FlagImpuesto { get; set; }
        public bool FlagCotizador { get; set; }
        public string IdTipoPagoSap { get; set; }
        public string ColetillaIva { get; set; }
    }
}
