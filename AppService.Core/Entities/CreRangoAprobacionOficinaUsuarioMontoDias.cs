﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CreRangoAprobacionOficinaUsuarioMontoDias
    {
        public decimal Id { get; set; }
        public short Oficina { get; set; }
        public string Usuario { get; set; }
        public decimal MontoDesde { get; set; }
        public decimal MontoHasta { get; set; }
        public int DiasDesde { get; set; }
        public int DiasHasta { get; set; }
        public bool Administradora { get; set; }
        public decimal? RmontoDesde { get; set; }
        public decimal? RmontoHasta { get; set; }
    }
}
