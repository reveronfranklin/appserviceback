﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CrmMtrEstado
    {
        public CrmMtrEstado()
        {
            CrmSeguimiento = new HashSet<CrmSeguimiento>();
        }

        public short IdEstado { get; set; }
        public string NombreEstado { get; set; }
        public bool FlagPendiente { get; set; }
        public bool FlagCerrada { get; set; }

        public virtual ICollection<CrmSeguimiento> CrmSeguimiento { get; set; }
    }
}
