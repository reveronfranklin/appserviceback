﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CrmMtrSeguimiento
    {
        public CrmMtrSeguimiento()
        {
            CrmSeguimiento = new HashSet<CrmSeguimiento>();
        }

        public short IdTipoSegumiento { get; set; }
        public string NombreSeguimiento { get; set; }
        public bool FlagUpdateData { get; set; }

        public virtual ICollection<CrmSeguimiento> CrmSeguimiento { get; set; }
    }
}
