﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CrmNotificacionActividades
    {
        public decimal IdNotificacion { get; set; }
        public long IdSeguimiento { get; set; }
        public string Para { get; set; }
        public string Cc { get; set; }
        public string Cuerpo { get; set; }
        public DateTime FechaHoraEnvio { get; set; }
    }
}
