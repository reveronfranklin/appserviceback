﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class CrmVisitaSemana
    {
        public short IdSemana { get; set; }
        public string NombreSemana { get; set; }
        public string AbreviadoSemana { get; set; }
        public short IdFrecuencia { get; set; }

        public virtual CrmVisitaFrecuencia IdFrecuenciaNavigation { get; set; }
    }
}
