﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class DeudorTipoRetencion
    {
        public decimal Id { get; set; }
        public string Cliente { get; set; }
        public string Nombre { get; set; }
        public string TipoRetencion { get; set; }
        public string IndicadorRetencion { get; set; }
    }
}
