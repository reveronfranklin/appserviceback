﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class Deudores
    {
        public string Cliente { get; set; }
        public string Nombre { get; set; }
        public string Rif { get; set; }
        public string Clase { get; set; }
    }
}
