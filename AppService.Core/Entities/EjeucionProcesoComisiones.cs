﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class EjeucionProcesoComisiones
    {
        public decimal Id { get; set; }
        public decimal IdEvaluacion { get; set; }
        public string Query { get; set; }
        public string Mensaje { get; set; }
        public DateTime? Inicio { get; set; }
        public DateTime? Fin { get; set; }
        public string Usuario { get; set; }
        public DateTime FechaEjecucion { get; set; }
    }
}
