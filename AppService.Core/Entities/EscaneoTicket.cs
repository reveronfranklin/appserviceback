﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class EscaneoTicket
    {
        public decimal Id { get; set; }
        public string Data { get; set; }
        public string UsuarioEscaneo { get; set; }
        public DateTime? FechaEscaneo { get; set; }
    }
}
