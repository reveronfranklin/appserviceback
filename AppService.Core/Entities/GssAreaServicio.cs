﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class GssAreaServicio
    {
        public GssAreaServicio()
        {
            GssTipoServicio = new HashSet<GssTipoServicio>();
        }

        public int IdAreaServicio { get; set; }
        public string NombreAreaServicio { get; set; }
        public short IdUnidadServicio { get; set; }
        public bool Activo { get; set; }
        public short IdCoordinador { get; set; }
        public short IdAprobador { get; set; }
        public short IdAprobadorGerencia { get; set; }
        public DateTime FechaCambio { get; set; }
        public int UsuarioCambio { get; set; }

        public virtual GssUnidadServicio IdUnidadServicioNavigation { get; set; }
        public virtual ICollection<GssTipoServicio> GssTipoServicio { get; set; }
    }
}
