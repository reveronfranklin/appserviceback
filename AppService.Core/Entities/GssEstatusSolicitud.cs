﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class GssEstatusSolicitud
    {
        public GssEstatusSolicitud()
        {
            GssSolicitud = new HashSet<GssSolicitud>();
        }

        public short IdEstatusSolicitud { get; set; }
        public string NombreEstatusSolicitud { get; set; }
        public string Abreviado { get; set; }
        public bool FlagCreada { get; set; }
        public bool FlagPorAprobar { get; set; }
        public bool FlagEnProceso { get; set; }
        public bool FlagCerrada { get; set; }
        public bool FlagCancelada { get; set; }
        public DateTime FechaCambio { get; set; }
        public int UsuarioCambio { get; set; }

        public virtual ICollection<GssSolicitud> GssSolicitud { get; set; }
    }
}
