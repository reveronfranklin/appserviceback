﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class GssMtrTipoDato
    {
        public string IdTipoDato { get; set; }
        public string MascaraUsuario { get; set; }
        public string MensajeUsuario { get; set; }
        public bool FlagValidar { get; set; }
        public short Orden { get; set; }
        public string TipoDatoDatatables { get; set; }
    }
}
