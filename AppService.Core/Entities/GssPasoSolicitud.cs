﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class GssPasoSolicitud
    {
        public short IdPasoSolicitud { get; set; }
        public long IdSolicitud { get; set; }
        public bool DatosGenerales { get; set; }
        public bool TareasServicios { get; set; }
        public bool CamposAdicionales { get; set; }
        public bool AdjuntosObservaciones { get; set; }

        public virtual GssSolicitud IdSolicitudNavigation { get; set; }
    }
}
