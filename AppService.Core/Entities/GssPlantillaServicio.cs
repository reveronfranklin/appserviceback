﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class GssPlantillaServicio
    {
        public GssPlantillaServicio()
        {
            GssTareaPlantillaServicio = new HashSet<GssTareaPlantillaServicio>();
        }

        public int IdPlantillaServicio { get; set; }
        public int IdServicio { get; set; }
        public int IdVariable { get; set; }
        public string FuncionBusqueda { get; set; }
        public short Secuencia { get; set; }
        public bool Requerido { get; set; }
        public bool Activo { get; set; }
        public bool? OcultaDuplicado { get; set; }
        public DateTime FechaCambio { get; set; }
        public int UsuarioCambio { get; set; }

        public virtual GssServicios IdServicioNavigation { get; set; }
        public virtual GssVariable IdVariableNavigation { get; set; }
        public virtual ICollection<GssTareaPlantillaServicio> GssTareaPlantillaServicio { get; set; }
    }
}
