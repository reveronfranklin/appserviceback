﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class GssServicioResponsable
    {
        public long IdServicioResponsable { get; set; }
        public int IdServicio { get; set; }
        public string IdTipoNomina { get; set; }
        public int IdResponsable { get; set; }
        public byte Secuencia { get; set; }
        public bool Inactivo { get; set; }
        public DateTime FechaCambio { get; set; }
        public int UsuarioCambio { get; set; }
    }
}
