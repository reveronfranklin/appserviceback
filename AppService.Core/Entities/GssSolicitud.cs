﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class GssSolicitud
    {
        public GssSolicitud()
        {
            GssPasoSolicitud = new HashSet<GssPasoSolicitud>();
            GssTarea = new HashSet<GssTarea>();
        }

        public long IdSolicitud { get; set; }
        public string TituloSolicitud { get; set; }
        public string DescripcionSolicitud { get; set; }
        public string JustificacionSolicitud { get; set; }
        public short IdUnidadServicio { get; set; }
        public short IdEstatusSolicitud { get; set; }
        public DateTime FechaSolicitud { get; set; }
        public DateTime FechaCambio { get; set; }
        public int UsuarioCambio { get; set; }

        public virtual GssEstatusSolicitud IdEstatusSolicitudNavigation { get; set; }
        public virtual GssUnidadServicio IdUnidadServicioNavigation { get; set; }
        public virtual ICollection<GssPasoSolicitud> GssPasoSolicitud { get; set; }
        public virtual ICollection<GssTarea> GssTarea { get; set; }
    }
}
