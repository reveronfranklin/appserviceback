﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class GssTareaComentario
    {
        public long IdComentarioTarea { get; set; }
        public long IdTarea { get; set; }
        public DateTime FechaComentario { get; set; }
        public string ComentarioTarea { get; set; }
        public short IdUsuario { get; set; }

        public virtual GssTarea IdTareaNavigation { get; set; }
    }
}
