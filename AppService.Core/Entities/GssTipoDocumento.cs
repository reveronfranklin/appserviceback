﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class GssTipoDocumento
    {
        public GssTipoDocumento()
        {
            GssAdjuntoTarea = new HashSet<GssAdjuntoTarea>();
        }

        public short IdTipoDocumento { get; set; }
        public string NombreDocumento { get; set; }
        public bool Inactivo { get; set; }
        public int IdUsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int IdUsuarioModificacion { get; set; }
        public DateTime FechaModificacion { get; set; }

        public virtual ICollection<GssAdjuntoTarea> GssAdjuntoTarea { get; set; }
    }
}
