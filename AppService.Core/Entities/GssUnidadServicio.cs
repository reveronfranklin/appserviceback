﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class GssUnidadServicio
    {
        public GssUnidadServicio()
        {
            GssAreaServicio = new HashSet<GssAreaServicio>();
            GssNivel = new HashSet<GssNivel>();
            GssSolicitud = new HashSet<GssSolicitud>();
            GssUnidadServicioCompañia = new HashSet<GssUnidadServicioCompañia>();
        }

        public short IdUnidadServicio { get; set; }
        public string NombreUnidadServicio { get; set; }
        public string Abreviado { get; set; }
        public bool Activo { get; set; }
        public string CuentaUnidadServicio { get; set; }
        public DateTime FechaCambio { get; set; }
        public int UsuarioCambio { get; set; }

        public virtual ICollection<GssAreaServicio> GssAreaServicio { get; set; }
        public virtual ICollection<GssNivel> GssNivel { get; set; }
        public virtual ICollection<GssSolicitud> GssSolicitud { get; set; }
        public virtual ICollection<GssUnidadServicioCompañia> GssUnidadServicioCompañia { get; set; }
    }
}
