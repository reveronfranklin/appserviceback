﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class GssUsuarioNivel
    {
        public int IdUsuarioNivel { get; set; }
        public int IdGrupoTrabajo { get; set; }
        public int IdUsuario { get; set; }
        public int IdNivel { get; set; }
        public int Secuencia { get; set; }
        public DateTime FechaCambio { get; set; }
        public int UsuarioCambio { get; set; }

        public virtual GssNivel IdNivelNavigation { get; set; }
    }
}
