﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class GssUsuarioPlantillaServicio
    {
        public int IdUsuarioPlantillaServicio { get; set; }
        public int IdUsuario { get; set; }
        public int IdServicio { get; set; }
        public int IdVariable { get; set; }

        public virtual GssServicios IdServicioNavigation { get; set; }
        public virtual GssVariable IdVariableNavigation { get; set; }
    }
}
