﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class MtrDirecciones
    {
        public decimal Id { get; set; }
        public string Codigo { get; set; }
        public string Rif { get; set; }
        public string Direccion { get; set; }
        public string Direccion1 { get; set; }
        public string Direccion2 { get; set; }
        public string Estado { get; set; }
        public string Municipio { get; set; }
        public string Nuevo { get; set; }
        public string UsuarioAprobacionDireccion { get; set; }
        public string FechaAprobacionDireccion { get; set; }
        public string PuntoReferencia { get; set; }
        public decimal Recnum { get; set; }
        public string Origen { get; set; }
        public bool? Inactivo { get; set; }
        public DateTime? FechaUpdate { get; set; }
        public string FuncionInterlocutor { get; set; }
        public string Telf1 { get; set; }
        public string Telf2 { get; set; }
        public string StmpAddr { get; set; }
        public string Zzsn01 { get; set; }
        public string Zzsn02 { get; set; }
        public string Zzsn03 { get; set; }
        public string Zzsn04 { get; set; }
        public string Zzsn05 { get; set; }
        public string Zzsn06 { get; set; }
        public string Ort02 { get; set; }
    }
}
