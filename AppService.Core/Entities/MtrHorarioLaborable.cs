﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class MtrHorarioLaborable
    {
        public short IdHorario { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fin { get; set; }
        public string Laborable { get; set; }
        public string PrimerBloque { get; set; }
        public string Almuerzo { get; set; }
    }
}
