﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class MtrPerfilCorreo
    {
        public decimal Id { get; set; }
        public string PerfilId { get; set; }
        public string Perfil { get; set; }
    }
}
