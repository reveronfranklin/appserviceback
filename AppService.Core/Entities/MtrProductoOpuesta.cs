﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class MtrProductoOpuesta
    {
        public decimal Id { get; set; }
        public string Producto { get; set; }
        public string MedidaFraccion { get; set; }
        public string MedidaMascara { get; set; }
        public decimal? Frecuencia { get; set; }
    }
}
