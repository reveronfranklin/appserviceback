﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class MtrProductoPapeles
    {
        public decimal Id { get; set; }
        public string Producto { get; set; }
        public int Parte { get; set; }
        public string Papel { get; set; }
        public decimal? Frecuencia { get; set; }
        public string Descripcion { get; set; }
    }
}
