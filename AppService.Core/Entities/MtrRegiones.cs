﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class MtrRegiones
    {
        public decimal Recnum { get; set; }
        public string CodigoRegion { get; set; }
        public string NombreRegion { get; set; }
        public string GerenteRegion { get; set; }
        public decimal OverrideRegion { get; set; }
        public string CodigoJde { get; set; }
    }
}
