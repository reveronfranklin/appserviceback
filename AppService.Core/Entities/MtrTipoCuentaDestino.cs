﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class MtrTipoCuentaDestino
    {
        public MtrTipoCuentaDestino()
        {
            MtrBancos = new HashSet<MtrBancos>();
        }

        public int IdTipoCuentaDestino { get; set; }
        public string NombreCuentaDestino { get; set; }

        public virtual ICollection<MtrBancos> MtrBancos { get; set; }
    }
}
