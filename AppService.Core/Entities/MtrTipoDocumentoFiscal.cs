﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class MtrTipoDocumentoFiscal
    {
        public MtrTipoDocumentoFiscal()
        {
            PrcNumeracionFiscal = new HashSet<PrcNumeracionFiscal>();
        }

        public string TipoDocumento { get; set; }
        public string Descripcion { get; set; }
        public bool ValidaNumero { get; set; }
        public int IdFormato { get; set; }

        public virtual ICollection<PrcNumeracionFiscal> PrcNumeracionFiscal { get; set; }
    }
}
