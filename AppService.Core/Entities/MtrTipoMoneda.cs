﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class MtrTipoMoneda
    {
        public MtrTipoMoneda()
        {
            AppGeneralQuotes = new HashSet<AppGeneralQuotes>();
            AppIngredientsPrymaryMtrMoneda = new HashSet<AppIngredients>();
            AppIngredientsSecundaryMtrMoneda = new HashSet<AppIngredients>();
            AppProductsPrymaryMtrMoneda = new HashSet<AppProducts>();
            AppProductsSecundaryMtrMoneda = new HashSet<AppProducts>();
        }

        public long Id { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<AppGeneralQuotes> AppGeneralQuotes { get; set; }
        public virtual ICollection<AppIngredients> AppIngredientsPrymaryMtrMoneda { get; set; }
        public virtual ICollection<AppIngredients> AppIngredientsSecundaryMtrMoneda { get; set; }
        public virtual ICollection<AppProducts> AppProductsPrymaryMtrMoneda { get; set; }
        public virtual ICollection<AppProducts> AppProductsSecundaryMtrMoneda { get; set; }
    }
}
