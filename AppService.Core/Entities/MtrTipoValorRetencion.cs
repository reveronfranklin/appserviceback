﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class MtrTipoValorRetencion
    {
        public string IdTipoValor { get; set; }
        public string NombreTipoValor { get; set; }
    }
}
