﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class OfdAprobacionDiseno
    {
        public long IdSolicitud { get; set; }
        public string EmailCliente { get; set; }
        public string Cargo { get; set; }
        public long? Cedula { get; set; }
        public string ObsConsultor { get; set; }
        public string ObsCliente { get; set; }
        public string UsuarioAgrega { get; set; }
        public DateTime FechaAgrega { get; set; }
        public decimal IdTicket { get; set; }
        public bool FlagDescargoDiseno { get; set; }

        public virtual OfdSolicitudDiseno IdSolicitudNavigation { get; set; }
    }
}
