﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class OfdConstruccionCotizacion
    {
        public string Cotizacion { get; set; }
        public int Renglon { get; set; }
        public int Propuesta { get; set; }
        public string IdVariable { get; set; }
        public int IdParte { get; set; }
        public string Valor { get; set; }
        public DateTime FechaRegistro { get; set; }
        public decimal Id { get; set; }
    }
}
