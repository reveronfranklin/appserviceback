﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class OfdDetalleSolicitudDiseno
    {
        public long IdDetalleSolicitud { get; set; }
        public long IdSolicitud { get; set; }
        public short NumeroAnotacion { get; set; }
        public string Anotacion { get; set; }
        public byte[] Imagen { get; set; }
        public string NombreImagen { get; set; }
        public string IdUsuario { get; set; }
        public DateTime FechaRegistro { get; set; }

        public virtual OfdSolicitudDiseno IdSolicitudNavigation { get; set; }
    }
}
