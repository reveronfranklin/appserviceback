﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class OfdEstatusFlujo
    {
        public OfdEstatusFlujo()
        {
            OfdSolicitudDocFiscal = new HashSet<OfdSolicitudDocFiscal>();
        }

        public string IdEstatus { get; set; }
        public string NombreEstatus { get; set; }
        public string FlagPendiente { get; set; }
        public string FlagAprobado { get; set; }
        public string FlagRechazado { get; set; }
        public string FlagEnviado { get; set; }
        public bool FlagMail { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string UsuarioRegistro { get; set; }

        public virtual ICollection<OfdSolicitudDocFiscal> OfdSolicitudDocFiscal { get; set; }
    }
}
