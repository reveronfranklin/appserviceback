﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class OfdFlujoEstacion
    {
        public short IdFlujoEstacion { get; set; }
        public short IdEstacionActual { get; set; }
        public short IdEstacionAprueba { get; set; }
        public short IdEstacionRechaza { get; set; }
        public DateTime FechaRegistro { get; set; }

        public virtual OfdMaestroEstacion IdEstacionActualNavigation { get; set; }
    }
}
