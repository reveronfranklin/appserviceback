﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class OfdMaestroEstacion
    {
        public OfdMaestroEstacion()
        {
            OfdSeguimiento = new HashSet<OfdSeguimiento>();
        }

        public short IdEstacion { get; set; }
        public short IdFlujo { get; set; }
        public string NombreEstacion { get; set; }
        public string AbreviadoEstacion { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string UsuarioRegistro { get; set; }

        public virtual OfdMaestroFlujo IdFlujoNavigation { get; set; }
        public virtual OfdFlujoEstacion OfdFlujoEstacion { get; set; }
        public virtual ICollection<OfdSeguimiento> OfdSeguimiento { get; set; }
    }
}
