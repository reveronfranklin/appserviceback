﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class OfdMaestroTipoRespaldo
    {
        public OfdMaestroTipoRespaldo()
        {
            OfdSolicitudDiseno = new HashSet<OfdSolicitudDiseno>();
        }

        public short IdTipoRespaldo { get; set; }
        public string TipoRespaldo { get; set; }

        public virtual ICollection<OfdSolicitudDiseno> OfdSolicitudDiseno { get; set; }
    }
}
