﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class OfdMaestroUbicacionMarginal
    {
        public OfdMaestroUbicacionMarginal()
        {
            OfdSolicitudDiseno = new HashSet<OfdSolicitudDiseno>();
        }

        public short IdUbicacionMarginal { get; set; }
        public string UbicacionMarginal { get; set; }

        public virtual ICollection<OfdSolicitudDiseno> OfdSolicitudDiseno { get; set; }
    }
}
