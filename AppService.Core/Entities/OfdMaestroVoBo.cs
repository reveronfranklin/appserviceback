﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class OfdMaestroVoBo
    {
        public OfdMaestroVoBo()
        {
            OfdVoBoDiseno = new HashSet<OfdVoBoDiseno>();
        }

        public short IdVoBo { get; set; }
        public short IdEstacion { get; set; }
        public short Orden { get; set; }
        public string Instruccion { get; set; }
        public string IdUsuario { get; set; }
        public DateTime FechaRegistro { get; set; }

        public virtual ICollection<OfdVoBoDiseno> OfdVoBoDiseno { get; set; }
    }
}
