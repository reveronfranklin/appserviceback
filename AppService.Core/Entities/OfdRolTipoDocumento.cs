﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class OfdRolTipoDocumento
    {
        public long IdRolTipoDocumento { get; set; }
        public int IdRol { get; set; }
        public short IdTipoDocumento { get; set; }

        public virtual SegRol IdRolNavigation { get; set; }
        public virtual OfdTipoDocumento IdTipoDocumentoNavigation { get; set; }
    }
}
