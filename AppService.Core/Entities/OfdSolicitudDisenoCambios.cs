﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class OfdSolicitudDisenoCambios
    {
        public long IdSolDisCam { get; set; }
        public long IdSolicitud { get; set; }
        public int IdCambio { get; set; }
        public DateTime FechaRegistro { get; set; }

        public virtual OfdCambioDiseno IdCambioNavigation { get; set; }
        public virtual OfdSolicitudDiseno IdSolicitudNavigation { get; set; }
    }
}
