﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class OfdSolicitudDocFiscal
    {
        public long IdSdf { get; set; }
        public long IdNumeracion { get; set; }
        public string IdEstatus { get; set; }
        public string EmailCliente { get; set; }
        public string Cargo { get; set; }
        public long Cedula { get; set; }
        public bool FlagSaltoNumeracion { get; set; }
        public string ObsvConsultor { get; set; }
        public string ObsvCliente { get; set; }
        public string UsuarioAgrega { get; set; }
        public DateTime FechaAgrega { get; set; }
        public decimal? IdTicket { get; set; }

        public virtual OfdEstatusFlujo IdEstatusNavigation { get; set; }
        public virtual PrcNumeracionFiscal IdNumeracionNavigation { get; set; }
    }
}
