﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class OfdVariablesEspecificacion
    {
        public string IdVariable { get; set; }
        public string NombreVariable { get; set; }
        public string FlagObligatorio { get; set; }
        public string FlagInactiva { get; set; }
        public string IdVariableMc { get; set; }
        public string FlagMultipleValor { get; set; }
        public string FlagGralParte { get; set; }
    }
}
