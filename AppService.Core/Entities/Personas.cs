﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class Personas
    {
        public int Id { get; set; }
        public string Primernombre { get; set; }
        public string Segundonombre { get; set; }
    }
}
