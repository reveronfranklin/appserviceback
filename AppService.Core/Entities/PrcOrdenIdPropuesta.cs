﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class PrcOrdenIdPropuesta
    {
        public decimal Id { get; set; }
        public long IdPropuesta { get; set; }
        public long? Orden { get; set; }
    }
}
