﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class PrcPlantillaNumeraciones
    {
        public long Id { get; set; }
        public int? Mascara { get; set; }
        public int? Prefijo { get; set; }
        public string PrefijoChar { get; set; }
        public decimal? MaximoNumero { get; set; }
        public decimal? Hasta { get; set; }
    }
}
