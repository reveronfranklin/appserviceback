﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class ReqAreaRequerimiento
    {
        public ReqAreaRequerimiento()
        {
            ReqTipoTarea = new HashSet<ReqTipoTarea>();
        }

        public int IdAreaRequerimiento { get; set; }
        public string DescripcionAreaRequerimiento { get; set; }
        public int IdOrigenRequerimiento { get; set; }
        public DateTime? FechaCambio { get; set; }
        public string UsuarioCambio { get; set; }

        public virtual ReqOrigenRequerimiento IdOrigenRequerimientoNavigation { get; set; }
        public virtual ICollection<ReqTipoTarea> ReqTipoTarea { get; set; }
    }
}
