﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class ReqCompañia
    {
        public ReqCompañia()
        {
            ReqOrigenRequerimiento = new HashSet<ReqOrigenRequerimiento>();
        }

        public string IdCompañia { get; set; }
        public string NombreCompañia { get; set; }
        public DateTime? FechaCambio { get; set; }
        public string UsuarioCambio { get; set; }

        public virtual ICollection<ReqOrigenRequerimiento> ReqOrigenRequerimiento { get; set; }
    }
}
