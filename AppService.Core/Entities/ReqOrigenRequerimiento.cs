﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class ReqOrigenRequerimiento
    {
        public ReqOrigenRequerimiento()
        {
            ReqAreaRequerimiento = new HashSet<ReqAreaRequerimiento>();
        }

        public int IdOrigenRequerimiento { get; set; }
        public string DescripcionOrigenRequerimiento { get; set; }
        public string IdCompañia { get; set; }
        public DateTime? FechaCambio { get; set; }
        public string UsuarioCambio { get; set; }

        public virtual ReqCompañia IdCompañiaNavigation { get; set; }
        public virtual ICollection<ReqAreaRequerimiento> ReqAreaRequerimiento { get; set; }
    }
}
