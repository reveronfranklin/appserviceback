﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class ReqTarea
    {
        public int IdTarea { get; set; }
        public string DescripcionTarea { get; set; }
        public int IdTipoTarea { get; set; }
        public DateTime? FechaCambio { get; set; }
        public string UsuarioCambio { get; set; }

        public virtual ReqTipoTarea IdTipoTareaNavigation { get; set; }
    }
}
