﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class ReqTipoTarea
    {
        public ReqTipoTarea()
        {
            ReqTarea = new HashSet<ReqTarea>();
        }

        public int IdTipoTarea { get; set; }
        public string DescripcionTipoTarea { get; set; }
        public int IdAreaRequerimiento { get; set; }
        public DateTime? FechaCambio { get; set; }
        public string UsuarioCambio { get; set; }

        public virtual ReqAreaRequerimiento IdAreaRequerimientoNavigation { get; set; }
        public virtual ICollection<ReqTarea> ReqTarea { get; set; }
    }
}
