﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class ReqVariables
    {
        public int IdVariable { get; set; }
        public string NombreVariable { get; set; }
        public int? IdTipoVariable { get; set; }
        public string TipoDato { get; set; }
        public string FuncionValidacion { get; set; }
        public DateTime? FechaCambio { get; set; }
        public string UsuarioCambio { get; set; }
    }
}
