﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class RetencionesCorregir
    {
        public string Sociedad { get; set; }
        public string Documento { get; set; }
        public string Ejercicio { get; set; }
        public string Posicion { get; set; }
        public string TipoRetenciones { get; set; }
        public string IndicadorRetenciN { get; set; }
        public string CertificadoRetencion { get; set; }
        public string FechaCertificadoRetencion { get; set; }
    }
}
