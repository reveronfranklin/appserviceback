﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class RrdejecutarSql
    {
        public decimal Id { get; set; }
        public string Proceso { get; set; }
        public string QuerySql { get; set; }
        public string Ejecutar { get; set; }
    }
}
