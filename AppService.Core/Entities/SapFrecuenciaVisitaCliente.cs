﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class SapFrecuenciaVisitaCliente
    {
        public string IdCliente { get; set; }
        public short IdFrecuenciaVisita { get; set; }
        public short IdSemanaVisita { get; set; }
        public short IdDiaVisita { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public DateTime FechaActualiza { get; set; }
        public string UsuarioActualiza { get; set; }
    }
}
