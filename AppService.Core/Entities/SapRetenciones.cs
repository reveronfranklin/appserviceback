﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class SapRetenciones
    {
        public decimal Id { get; set; }
        public string TipodeRetencion { get; set; }
        public string Denominacion { get; set; }
        public string IndicadordeRetencion { get; set; }
        public double? Porcentaje { get; set; }
        public double? AsigancionCuentadebe { get; set; }
        public double? AsigancionCuentahaber { get; set; }
        public double? TransaccionLegacy { get; set; }
    }
}
