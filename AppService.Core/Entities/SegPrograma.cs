﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class SegPrograma
    {
        public int IdPrograma { get; set; }
        public string NombrePrograma { get; set; }
        public string Descripcion { get; set; }
        public string TipoPrograma { get; set; }
        public bool? ParaMenu { get; set; }
        public string Link { get; set; }
        public string FlagRequerimiento { get; set; }
        public byte IdAreaNegocio { get; set; }
        public short ServidorReporte { get; set; }
        public short ServidorPublico { get; set; }
    }
}
