﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class SegRolModulo
    {
        public int IdRolModulo { get; set; }
        public int IdRol { get; set; }
        public int IdModulo { get; set; }
    }
}
