﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class SegTicket
    {
        public decimal Id { get; set; }
        public string TicketSha1 { get; set; }
        public bool FlagUsado { get; set; }
        public string IdUsuario { get; set; }
        public string IpUsuario { get; set; }
        public DateTime FechaHoraCreacion { get; set; }
        public DateTime? FechaHoraUso { get; set; }
        public DateTime? FechaHoraGetIp { get; set; }
    }
}
