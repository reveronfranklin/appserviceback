﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class Sysfile
    {
        public decimal Id { get; set; }
        public string LinkServer { get; set; }
        public string LinkServerProduccion { get; set; }
        public string WebServer { get; set; }
        public string Servidor { get; set; }
        public string MailserverName { get; set; }
        public string MailserverType { get; set; }
        public decimal? MailserverPort { get; set; }
        public short? PrimerDiaSemana { get; set; }
        public string DirVirtualOd { get; set; }
    }
}
