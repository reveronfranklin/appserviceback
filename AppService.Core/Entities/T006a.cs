﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class T006a
    {
        public T006a()
        {
            MtrProducto = new HashSet<MtrProducto>();
        }

        public string Mandt { get; set; }
        public string Spras { get; set; }
        public string Msehi { get; set; }
        public string Mseh3 { get; set; }
        public string Mseh6 { get; set; }
        public string Mseht { get; set; }
        public string Msehl { get; set; }

        public virtual ICollection<MtrProducto> MtrProducto { get; set; }
    }
}
