﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class TEnEntradas
    {
        public decimal Id { get; set; }
        public decimal IdSolicitud { get; set; }
        public int LineaNegocio { get; set; }
        public string IdVariables { get; set; }
        public int Item { get; set; }
        public string Valor { get; set; }
        public string FuncionDeValidacion { get; set; }
        public string MensajeValidacion { get; set; }
        public string FuncionDeBusqueda { get; set; }
        public string Orden { get; set; }
        public string FuncionDeBusquedaValor { get; set; }
        public string Producto { get; set; }

        public virtual TPaVariables IdVariablesNavigation { get; set; }
    }
}
