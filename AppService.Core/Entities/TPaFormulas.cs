﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class TPaFormulas
    {
        public decimal Id { get; set; }
        public string IdVariables { get; set; }
        public int LineaNegocio { get; set; }
        public string Formula { get; set; }
        public decimal? OrdenCalculo { get; set; }
        public string UsuarioCreacion { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string UsuarioModifico { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string DescripcionFormula { get; set; }
    }
}
