﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class TPaModeloCalculo
    {
        public TPaModeloCalculo()
        {
            TPaPlantillaEntradas = new HashSet<TPaPlantillaEntradas>();
        }

        public decimal Id { get; set; }
        public int CodigoModelo { get; set; }
        public string DescripcionModelo { get; set; }
        public string UsuarioCreacion { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string UsuarioModifico { get; set; }
        public DateTime? FechaModificacion { get; set; }

        public virtual ICollection<TPaPlantillaEntradas> TPaPlantillaEntradas { get; set; }
    }
}
