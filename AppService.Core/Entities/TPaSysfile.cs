﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class TPaSysfile
    {
        public int Id { get; set; }
        public string DestinatarioFuncional { get; set; }
        public string CopiaFuncional { get; set; }
        public string SegundaCopiaFuncional { get; set; }
        public string DestinatarioTecnico { get; set; }
        public string CopiaTecnico { get; set; }
        public string UsuarioCreacion { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string UsuarioModifico { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public decimal? ConsecutivoOrdenPlantilla { get; set; }
    }
}
