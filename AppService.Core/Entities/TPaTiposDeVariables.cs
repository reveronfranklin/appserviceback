﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class TPaTiposDeVariables
    {
        public TPaTiposDeVariables()
        {
            TPaVariables = new HashSet<TPaVariables>();
        }

        public decimal Id { get; set; }
        public int IdTipoVariable { get; set; }
        public string Descripcion { get; set; }
        public string Calculo { get; set; }
        public string Entrada { get; set; }
        public string Constante { get; set; }
        public string Busqueda { get; set; }
        public string Acumulador { get; set; }
        public string AcumuladorGeneral { get; set; }
        public string UsuarioCreacion { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string UsuarioModifico { get; set; }
        public DateTime? FechaModificacion { get; set; }

        public virtual ICollection<TPaVariables> TPaVariables { get; set; }
    }
}
