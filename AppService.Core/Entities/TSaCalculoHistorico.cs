﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class TSaCalculoHistorico
    {
        public decimal Id { get; set; }
        public decimal IdSolicitud { get; set; }
        public int LineaNegocio { get; set; }
        public string IdVariables { get; set; }
        public int Item { get; set; }
        public string Formula { get; set; }
        public decimal? OrdenCalculo { get; set; }
        public decimal? Valor { get; set; }
        public string DescripcionFormula { get; set; }
        public DateTime? FechaCalculo { get; set; }
        public string Orden { get; set; }
        public string Producto { get; set; }
        public string Qry { get; set; }
        public decimal? TiempoEjecucion { get; set; }
    }
}
