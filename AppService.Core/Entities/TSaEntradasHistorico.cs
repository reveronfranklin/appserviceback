﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class TSaEntradasHistorico
    {
        public decimal Id { get; set; }
        public decimal IdSolicitud { get; set; }
        public int LineaNegocio { get; set; }
        public string IdVariables { get; set; }
        public int Item { get; set; }
        public string Valor { get; set; }
        public DateTime? FechaCalculo { get; set; }
        public string Orden { get; set; }
        public string Producto { get; set; }
        public string FuncionDeBusqueda { get; set; }
    }
}
