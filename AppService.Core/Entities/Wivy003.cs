﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace AppService.Core.Entities
{
    public partial class Wivy003
    {
        public decimal Recnum { get; set; }
        public short Codigo { get; set; }
        public string Descripcion { get; set; }
        public short DiasVcto { get; set; }
        public string AprobCredM { get; set; }
        public string CodJde { get; set; }
        public string Inactivo { get; set; }
        public string RequiereLimiteCredito { get; set; }
        public decimal? PorcRequerido { get; set; }
    }
}
