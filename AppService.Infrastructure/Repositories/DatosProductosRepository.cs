﻿using AppService.Core.DataContratosStock;
using AppService.Core.Interfaces;
using AppService.Infrastructure.DataContratosStock;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Infrastructure.Repositories
{
    public class DatosProductosRepository: IDatosProductosRepository
    {

        private readonly ContratosStockContext _context;
     
        public DatosProductosRepository(ContratosStockContext context)
        {
            _context = context;
        
        }

        public async Task<DatosProductos> GetByNumCotProducto(decimal numCot ,string producto)
        {
            return await _context.DatosProductos.Where(x => x.NumCot == numCot && x.Codigo==producto).FirstOrDefaultAsync();
        }

        public async Task Add(DatosProductos entity)
        {
            await _context.DatosProductos.AddAsync(entity);


        }



        public void Update(DatosProductos entity)
        {
            _context.DatosProductos.Update(entity);

        }

        public async Task Delete(decimal numCot, string producto)
        {
            DatosProductos entity = await GetByNumCotProducto(numCot,  producto);
            _context.DatosProductos.Remove(entity);

        }


    }
}
